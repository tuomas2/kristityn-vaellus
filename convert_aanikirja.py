# encoding:utf-8


#Äänikirjan muodostamista varten tämä skripti

from HTMLParser import HTMLParser
class MyHTMLParser(HTMLParser):
    result = ""
    skip = False
    def handle_starttag(self, tag, attrs):
        if tag == "a" or tag == "title":
            self.skip = True
        if tag == "h2":
            self.result += "Otsikko: "
        if tag == "h3":
            self.result += "Alatsikko: "
        if tag == "p":
            self.result += "silencebreak "
    def handle_endtag(self, tag):
        if tag == "a" or tag == "title":
            self.skip = False
        if tag == "h2" or tag == "h3":
            self.result += ". silencebreak"
    def handle_data(self, data):
        if not self.skip:
            self.result += data
    def handle_entityref(self,data):
        if data == "auml":
            self.result += "ä"
        elif data == "ouml":
            self.result += "ö"
        elif data == "Auml":
            self.result += "Ä"
        elif data == "Ouml":
            self.result += "Ö"
#        else:
#            self.result += "{%s}"%data
def parsefile(i,o):
    parser = MyHTMLParser()
    import sys
    parser.feed(open(i).read())

    r = parser.result
    r=r.replace(" , "," ")
    r=r.replace("\"","")
    r=r.replace(";","")
    r=r.replace("  "," ")
    r=r.replace("  "," ")
    r=r.replace(" .",". ")
    r=r.replace("?,","?")
    r=r.replace(" ?,","? ")
    r=r.replace("'","")
    r=r.replace(".,",".")
    r=r.replace("j. n. e.","ja niin edelleen")
    r=r.replace(" hra "," herra ")
    r=r.replace(" - ",", ")
    r=r.replace("-,",", ")
    r=r.replace(" ,",", ")
    r=r.replace("  "," ")
    r=r.replace("\n\n","\n")
    r=r.replace("\n\n","\n")
    r=r.replace("\n\n","\n")
    r=r.replace("\n\n","\n")
    r=r.replace("\n\n","\n")
    r=r.replace("\n\n","\n")
    r=r.replace("\n\n","\n")
    r=r.replace("\n\n","\n")
    r=r.replace("\n\n","\n")
    r=r.replace("\n\n","\n")
    r=r.replace("\n\n","\n")
    r=r.replace("  "," ")
    r=r.replace("  "," ")
    r=r.replace("  "," ")
    r=r.replace("  "," ")
    r=r.replace("  "," ")
    r=r.replace("  "," ")
    r=r.replace("  "," ")
    r=r.replace("  "," ")
    r=r.replace("  "," ")
    r=r.replace("\xc4","Ä")
    r=r.replace("\x85","")
    r=r.replace("\xe4","ä")
    #r=r.replace("\xc3\xa4","ä")
    of = open(o,"w")
    of.write(r)
import os
files = os.listdir("html")
for i in files:
    if i[-4:] == "html":
        parsefile("html/"+i,"txt/"+i[:-5]+".txt")

