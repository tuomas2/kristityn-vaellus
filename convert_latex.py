# encoding:utf-8
import os
import re
import subprocess
#from html.parser import HTMLParser
from HTMLParser import HTMLParser
#xrange = range
from data import scenefigtable, transdict, headers, footer, simple_replace
erisnimet = open("erisnimet.txt").read().split("\n")

htmlfiles = os.listdir("html")
htmlfiles.sort()

charfigs = os.listdir("pilgrimcharacters")
charfigs.sort()

scenefigs = os.listdir("pilgrimscenes")
scenefigs.sort()

scenesizemult={}
charsizemult={4:1.2,12:2.0/1.3,11:1.5/1.3}


scenedict = {}
for i in scenefigs:
    nid = i.find("_")
    num = int(i[:nid])
    name = i[nid:].replace("_"," ")
    scenedict[num] = (i,name)

chardict = {}
for i in charfigs:
    nid = i.find("_")
    num = int(i[:nid])
    name = i[nid:].replace("_"," ")
    chardict[num] = (i,name)

def translate(s):
    try:
        return transdict[s]
    except:
        return s

def bigsublettersize(s):
    s = s.lower().replace("Ä","ä").replace("Ö","ö").capitalize()
    if s[:2] == "ö":
        s="Ö"+s[2:]
    if s[:2] == "ä":
        s="Ä"+s[2:]

    for i in erisnimet:
        pieni = i
        iso = i.capitalize()
        if iso[:2] == "ö":
            iso="Ö"+iso[2:]
        if iso[:2] == "ä":
            iso="Ä"+iso[2:]
        s=re.sub(r'\b%s\b'%pieni, iso, s, flags=re.MULTILINE) #TODO: Tarkista VanHurskas
    return s

class MyHTMLParser(HTMLParser):
    result = ""
    skip = False
    otsikko = False
    laulu = False
    def handle_starttag(self, tag, attrs):
        attrs = dict(attrs)
        if tag == "title":
            self.skip = True
        if tag == "a":
            self.result += "\\bibleref{"
        if tag == "h2":
            self.result += "\n\n\iso{"
            self.otsikko = True
        if tag == "h3":
            self.result += "\n\n\pieni{"
            self.otsikko = True
        if tag == "p":
            self.result += "\n\n"
            if "class" in attrs and attrs["class"] == "laulu":
                self.result += "\\begin{verse}\n"
                self.laulu = True
    def handle_endtag(self, tag):
        if tag == "p" and self.laulu:
            self.result += "\n\\end{verse}\n"
            self.laulu = False
        if tag == "title":
            self.skip = False
        if tag == "a":
            self.result += "}"
        if tag == "h2" or tag == "h3":
            self.result += "}\n"
            self.otsikko = False
    def handle_data(self, data):
        if not self.skip:
            if self.laulu:
                data = data.replace("\n","\\\\\n")
            self.result += data
           
    def handle_entityref(self,data):
        if data == "auml":
            self.result += "ä"
        elif data == "ouml":
            self.result += "ö"
        elif data == "Auml":
            self.result += "Ä"
        elif data == "Ouml":
            self.result += "Ö"



def parsefile(i,o, of):
    parser = MyHTMLParser()
    parser.feed(open(i).read())
    r = parser.result#.decode('utf-8')
    r=r.replace(" , ",", ")
    r=r.replace(" \" ","\" ")
    r=r.replace("\"","''")
    r=r.replace(" .",". ")
    r=r.replace("?,","?")
    r=r.replace(" ?,","? ")
    r=r.replace(".,",".")
    r=r.replace(" - "," -- ")
    r=r.replace("-,","--, ")
    r=r.replace(" ,",", ")

    for i in xrange(10):
        r=r.replace("  "," ")

    r=r.replace("\n\\\\\n","\n")
    r=r.replace("\x85","")

    r=r.replace("\xc4","Ä")
    r=r.replace("\xe4","ä")
    r=r.replace("\\bibleref{Seuraava}","")
    r=r.replace("\\bibleref{Edellinen}","")

    biblerefmatch = re.compile(r"\\bibleref{([^}]+)}[;, ]*\\bibleref{")
    r = biblerefmatch.sub(r"\\bibleref{\1; ",r)
    r = biblerefmatch.sub(r"\\bibleref{\1; ",r)
    r = biblerefmatch.sub(r"\\bibleref{\1; ",r)
    r = biblerefmatch.sub(r"\1",r)

    biblerefmatch = re.compile(r"(\\bibleref{[^}]+}) ")
    r = biblerefmatch.sub(r"\1",r)
    biblerefmatch = re.compile(r" (\\bibleref{[^}]+})")
    r = biblerefmatch.sub(r"\1",r)
    isopienimatch = re.compile(r"\\(iso|pieni){([^}]+)}")

    def testfunc(match):
        g= match.groups()
        return "\\" + g[0] + "{" + bigsublettersize(g[1]) + "}"
    r = isopienimatch.sub(testfunc, r)


    #ett'en -> etten
    #ett'ei -> ettei
    #ett'ette -> ettette
    #etteivät
    r = re.sub(r"(\b[Ee])(tt')", r"\1tt", r, flags=re.MULTILINE)

    #TODO:
    #r = re.sub(r"(\b)(la')", r"\1lak",r, flags=re.MULTILINE)
    #r = re.sub(r"(\b[yY])lönkat", r"\1lenkat", r, flags=re.MULTILINE)

    #NIMIÄ
    #Moseksen -> Mooseksen
    #Sionin -> Siionin
    #Davidkin -> Daavidkin
    #Josefin
    #Franskan


    #OUTOJA
    #kallotellen -> valittaen
    #p. 25 'sanoi hän'
    #kettää -> pois (pettää ja kettää)
    #paraasta (p.31) -> pois (sana sanasta ja kaksi paraasta)
    #tuonottain
    #avu
    #vaarin-otettaviksi->vaarin otettaviksi
    #ylitsekäymärit -> luopiot
    #aivinainen -> pellavavaate
    #sotisopaan -> sota-asuun
    #viine  -- vaikka viine kolisisi
    #luihkaa
#        r'j\. n\. e\.': 'jne.',
#        r'j\.n\.e\.': 'jne\.',
#        r'y\. m\.': 'ym.',
#        r's\. o\.': 'so.',

    #lyhenteet pitää korvata erikseen
    r = r.replace('j. n. e.', 'jne.')
    r = r.replace('j.n.e.', 'jne.')
    r = r.replace('y. m.', 'ym.')
    r = r.replace('s. o.', 'so.')

    #Miks'ette -> Miksi ette
    #Siks'ette -> Siksi ette
    #Siks'että
    #Miks'ei
    r = re.sub(r"(\b[mMSs])iks'", r"\1iksi ", r, flags=re.MULTILINE)
    r = re.sub(r"(\b[mM])in'", r"\1in ",r, flags=re.MULTILINE)
    #r = re.sub(r"(\b)([kK])(osk')", r"\1\2osk ",r, flags=re.MULTILINE)

    #Vaikk'en -> vaikken
    #Vaikk'eivät -> vaikkeivät
    #Vaikk'ei vaikk'emme
    r = re.sub(r"(\b[vV])aikk'", r"\1aikk", r, flags=re.MULTILINE)

    r = r.decode('utf-8')

    for key,value in simple_replace.iteritems():
        r = re.sub((r'\b%s\b'%key).decode('utf-8'), value.decode('utf-8'), r, flags=re.MULTILINE|re.UNICODE)

    # .ref. -> .ref
    #nouseva."1. Samassa -> nouseva."1 Samassa
    r = re.sub(r"\.''(\\bibleref{[^}]*}).", r".''\1", r, flags=re.MULTILINE)

    #Elämän koreus8ja että saan ottaa heidät kaikki
    #solvaus1ja häväistys
    r = re.sub(r"([^\b]\\bibleref{[^}]*})([^\b])", r"\1 \2", r, flags=re.MULTILINE)
    r = r.replace(' . ', '. ')
    of.write(r)

def main():
    nodiff =True #False
    try:
        os.rename('kirja.tex', 'kirja_previous.tex')
    except OSError:
        nodiff = True

    from codecs import open
    of = open("kirja.tex","w", encoding='utf-8')
    of.write(headers)

    for i in htmlfiles:
        if i[-4:] == "html":
            print('Processing %s'%i)
            for fig in scenefigtable[i][0]:
                m = 1.0
                if fig in scenesizemult:
                    m = scenesizemult[fig]
                fname,name = scenedict[fig]
                of.write(("""\\begin{figure}[h]\centering
                            \includegraphics[width=%f\columnwidth]{pilgrimscenes/%s}
                            \caption{%s}
                            \end{figure}
                            """%(0.95*m,fname[:-4],translate(name[:-4].strip().capitalize()))).decode('utf-8'))
            howmany = len(scenefigtable[i][1])
            if howmany>0:
                of.write("\\begin{figure}[h]\centering")
                caption=""
                for fig in scenefigtable[i][1]:
                    m = 1.0
                    if fig in charsizemult:
                        m = charsizemult[fig]
                    fname,name = chardict[fig]
                    if howmany == 1:
                        m *= 1.3
                    of.write("""\includegraphics[width=%f\columnwidth]{pilgrimcharacters/%s}
                                """%(0.35*m,fname[:-4]))
                    assert howmany < 3
                    caption += name[:-4].title() + " and "
                of.write(("\caption{%s}"%translate(caption[:-4].strip().replace("&"," and "))).decode('utf-8'))
                of.write("\end{figure}\n")
            parsefile("html/"+i,"txt/"+i[:-5]+".txt", of)
    of.write(footer)
    of.close()
    if not nodiff:
        subprocess.call('latexdiff kirja_previous.tex kirja.tex > diff.tex', shell=True)
        subprocess.call('pdflatex diff.tex', shell=True)
        subprocess.call('pdflatex diff.tex', shell=True)
    else:
        subprocess.call('pdflatex kirja.tex', shell=True)
        subprocess.call('pdflatex kirja.tex', shell=True)
        subprocess.call('pdflatex kirja.tex', shell=True)

main()

