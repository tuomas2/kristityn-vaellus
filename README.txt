Jos vain haluat lueskella Kristityn vaellus-kirjaa, voit ladata sen pdf:nä 

[tästä linkistä](https://drive.google.com/file/d/0B1gzw4XUMye6LTBCM0JacEl1S1k/view)




Kristityn vaellus -kirjan kääntäminen pdf-muotoon lähdemateriaaleista:

1. Aja komento
   python convert_latex.py
   Tämä muodostaa kirja.tex-tiedoston html-tiedostoista. 

2. Aja komento
   pdflatex kirja.tex
   (aja pari kertaa niin saat sisällysluettelonkin oikein). 
   Nyt sinulla on kirja.pdf käännettynä. Lisäohjeita Googlella ja kysymällä ;) 


Taitto (eli python-koodit ja latex-pohja) 2013 (C) Tuomas A., 
sähköposti tuma at cc piste jyu toinen piste fi.
Lisenssi näille GPL (lue tiedosto COPYING). 
Tekstit Public domain.

T: Tuomas